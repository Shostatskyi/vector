#include <iostream>
#include "Vector.h"
using namespace std;

// Why in case of using template a separate .cpp file for definition of methods is not working?

void main()
{
	Vector<int> v2, v1(5);
	for (int i = 0; i < 5; i++){
		v2.push_back(i);
		v1[i] = i;
		cout << v2[i] << ' ';
	}

	cout << endl;
	cout << v1.pop_back() << endl;
	cout << v2.empty() << endl;

	system("pause");
}
