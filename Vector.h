#pragma once
#include <iostream>
#include <assert.h>
using namespace std;

template <class T>
class Vector 
{
    private:
		size_t _size;
		T*     _arr;

    public:
		Vector() : _size(0), _arr(nullptr) {}
		Vector(int s) : _size(s), _arr(new T[s]) {}
		Vector(Vector const &vec);
		~Vector() { delete[] _arr; };

		T& operator[] (int n);
		void push_back(T val);
		T pop_back(); 
		bool empty();
};

template <class T>
Vector<T>::Vector(Vector<T> const &vec)
{
	_size = vec._size, _arr = new T[_size];
	T ptr_init = vec, ptr_input = vec._arr;
	for (int i = 0; i < _size; ptr_init, ptr_input++, i++)
		*ptr_init = *ptr_input;
} 

template <class T>
T& Vector<T>::operator[] (int n)
{
	assert(n > -1 && n < _size);
	return _arr[n];
}

template <class T>
void Vector<T>::push_back(T val)
{
	_arr = (T*) realloc(_arr, sizeof(T)* ++_size);
	_arr[_size - 1] = val;
}

template <class T>
T Vector<T>::pop_back()
{
	assert(_size);
	T tmp = _arr[_size - 1];
	_arr = (T*)realloc(_arr, sizeof(T)* --_size);
	return tmp;
}

template <class T>
bool Vector<T>::empty()
{
	return _size ? false : true;
}



